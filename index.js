const express = require("express"); // Tương tự : import express from "express";
const path = require("path");

// Khởi tạo Express App
const app = express();

const port = 8005;

app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/sample.06restAPI.order.pizza365.v2.0.html"))
})

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})